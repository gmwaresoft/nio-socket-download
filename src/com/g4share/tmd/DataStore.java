package com.g4share.tmd;

import java.util.ArrayList;
import java.util.List;

public class DataStore implements DataStorable, DataRetrievable{
	private List<byte[]> dataList;
	
	@Override
	public void add(byte[] data, boolean finished) {
		if (dataList == null) dataList = new ArrayList<byte[]>();
		dataList.add(data);
	}

	@Override
	public void error(Throwable ex) {
		dataList = null;
	}

	@Override
	public byte[] getStore() {
		int dataSize = 0;
		for(int i = 0; i < dataList.size(); i++){
			dataSize += dataList.get(i).length;
		}
		
		byte[] data = new byte[dataSize];
		int currentPosition = 0;

		for(int i = 0; i < dataList.size(); i++){
			System.arraycopy(dataList.get(i), 0, data, currentPosition, dataList.get(i).length);
			currentPosition += dataList.get(i).length;
		}
		
		return data;
	}
}

package com.g4share.tmd;

public interface DataStorable extends DataRetrievable {
	public void add(byte[] data, boolean finished);
	public void error(Throwable ex);
}

package com.g4share.tmd;

import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousSocketChannel;
import java.nio.channels.CompletionHandler;
import java.util.Arrays;
import java.util.concurrent.Future;

public class AsynchReader implements DataReadable {
	private DataStorable dataStorable;
	
	private static final int READ_SIZE = 10;
	
	public AsynchReader(DataStorable dataStorable){
		this.dataStorable = dataStorable;
	}
	
	@Override
	public Future<DataRetrievable> read(AsynchronousSocketChannel asyncChannel) {
	    final ByteBuffer buffer = ByteBuffer.allocate(READ_SIZE);
	    AsynchCallBack asynchCallBack = new AsynchCallBack(dataStorable, asyncChannel, buffer);
	    return asynchCallBack.read();
	}
	
		
	static class AsynchCallBack implements CompletionHandler<Integer, Void>{
		private final DataStorable dataStorable;
		private final AsynchronousSocketChannel asyncChannel;
		private final ByteBuffer buffer;	
 
		private CompletitionFuture<DataRetrievable> completitionFuture;
		
    	public AsynchCallBack(DataStorable dataStorable,
				AsynchronousSocketChannel asyncChannel, 
				ByteBuffer buffer) {
			this.dataStorable = dataStorable;
			this.asyncChannel = asyncChannel;
			this.buffer = buffer;
			
			completitionFuture = new CompletitionFuture<>();
		}

		public CompletitionFuture<DataRetrievable> read(){
    	    asyncChannel.read(buffer, null, this);	
    	    return completitionFuture;
    	}

        @Override 
        public void completed(Integer result, Void attachment) {
            final int size = result.intValue();

            if(size != -1){
            	buffer.flip();
            	
            	boolean finished = false;
            	if (dataStorable != null){
            		finished = size < buffer.capacity();
            		
            		byte[] arrData = buffer.array();
            		if (finished){
            			arrData = Arrays.copyOf(arrData, size); 
            		}
            		
            		dataStorable.add(arrData.clone(), finished);
            	}
            	
            	buffer.clear();
            	if (!finished) read();
            	else {
                	completitionFuture.complete(dataStorable);
            	}
            } else {
            	if (dataStorable != null){
            		dataStorable.add(new byte[0], true);
            	}
            	completitionFuture.complete(dataStorable);
            }
        }

        @Override 
        public void failed(Throwable ex, Void attachment) { 
        	if (dataStorable != null){
        		dataStorable.error(ex);
        	}
        }	
	}
}
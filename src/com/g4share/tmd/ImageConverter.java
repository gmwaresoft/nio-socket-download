package com.g4share.tmd;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;

import javax.imageio.ImageIO;

public class ImageConverter implements DataConverter<BufferedImage> {

	@Override
	public BufferedImage convert(byte[] data) throws IOException {
		BufferedImage image = ImageIO.read(new ByteArrayInputStream(data));
		return image;
	}	
}

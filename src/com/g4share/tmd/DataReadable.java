package com.g4share.tmd;

import java.nio.channels.AsynchronousSocketChannel;
import java.util.concurrent.Future;

public interface DataReadable {
	public Future<DataRetrievable> read(AsynchronousSocketChannel asyncChanel);
}

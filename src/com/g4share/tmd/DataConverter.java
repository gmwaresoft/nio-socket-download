package com.g4share.tmd;

import java.io.IOException;

public interface DataConverter<T> {
	public T convert(byte[] data) throws IOException;
}

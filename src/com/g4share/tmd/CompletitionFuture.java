package com.g4share.tmd;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class CompletitionFuture<T> implements Future<T> {
		 
	private static enum State {WAITING, DONE, INTRERUPTED, CANCELLED}
	 
	private final BlockingQueue<T> queue = new ArrayBlockingQueue<>(1);
	private volatile State state = State.WAITING;
	 
	@Override
	public boolean cancel(boolean mayInterruptIfRunning) {
		state = State.CANCELLED;
		return true;
	}
	 
	@Override
	public boolean isCancelled() {
		return state == State.CANCELLED;
	}
	 
	@Override
	public boolean isDone() {
		return state == State.DONE 
				|| state == State.INTRERUPTED;
	}
	 
	@Override
	public T get() throws InterruptedException, ExecutionException {
		return this.queue.take();
	}
	 
	@Override
	public T get(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
		final T reply = queue.poll(timeout, unit);
		if (reply == null) {
			throw new TimeoutException();
		}
		return reply;
	}
	 
	public void complete(T result) {
		try {
			queue.put(result);
	        state = State.DONE;
		} catch (InterruptedException e) {
	        state = State.INTRERUPTED;
		}
	}
}
package com.g4share.tmd;

public interface DataRetrievable {
	public byte[] getStore();
}

package test.com.g4share.tmd;

import static org.junit.Assert.*;

import java.awt.Image;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.channels.AsynchronousSocketChannel;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.g4share.tmd.AsynchReader;
import com.g4share.tmd.DataReadable;
import com.g4share.tmd.DataRetrievable;
import com.g4share.tmd.DataStore;
import com.g4share.tmd.ImageConverter;

public class TestImageReader {

	private final static String FILE_NAME = "C:/Windows/Web/Wallpaper/Landscapes/img7.jpg";
    private ServerSocket serverSocket;
    int serverPort = 0;
    
	@Before 
	public void setUp() throws Exception {
		serverSocket = new ServerSocket(serverPort);
		serverPort = serverSocket.getLocalPort();
    	System.out.println("server listening at port " + serverPort);

        new Thread() {
        	@Override
            public void run() {
        		while (true){
        			try(Socket sock = serverSocket.accept()){
        	        	System.out.println("client socket accepted. ");

        				OutputStream os = sock.getOutputStream();
        				byte[] data = getFileContent(FILE_NAME);
        				os.write(data);
        				os.flush();
        			} catch (Exception e) {
        				break; //actually it is a unit test and we need only one connection to process
        			}
        		}
            }
        }.start();
 	}
	
	private byte[] getFileContent(String fileName) throws FileNotFoundException, IOException{
		File file = new File(fileName);
		byte[] data  = new byte[(int) (file.length() + 1)];
		
		try(FileInputStream fis = new FileInputStream(file);
				BufferedInputStream bis = new BufferedInputStream(fis)){
			bis.read(data, 0, data.length);
		}
		return data;
	}

	@After
	public void tearDown() throws Exception {
        if (serverSocket != null) {
        	serverSocket.close();
        	serverSocket = null;
        	System.out.println("server socket at port " + serverPort + " closed.");
        }
    }
        
	@Test
	public void readFromSochet() throws IOException, InterruptedException{		
		read(serverPort);
	}
	
	@Test
	public void readFileAndCheck() throws IOException, InterruptedException, ExecutionException{		
		byte[] fileData = getFileContent(FILE_NAME);
		
		Future<DataRetrievable> future = read(serverPort);
		DataRetrievable dataRetrievable = future.get();
		byte[] socketData = dataRetrievable.getStore();
		
		assertEquals("Files length are different", fileData.length, socketData.length);
		for(int i = 0; i < socketData.length; i++){
			assertEquals("difference at position " + i, fileData[i], socketData[i]);
		}
	}
	
	@Test
	public void testDataFileIsImage() throws IOException, InterruptedException, ExecutionException{		
		Future<DataRetrievable> future = read(serverPort);
		DataRetrievable dataRetrievable = future.get();
		byte[] socketData = dataRetrievable.getStore();
		
		ImageConverter converter = new ImageConverter();
		Image image = converter.convert(socketData);
		assertNotNull("Loaded data is not an image.", image);
	}
	
	private Future<DataRetrievable> read(int port) throws InterruptedException, IOException{
		AsynchronousSocketChannel asyncChanel = AsynchronousSocketChannel.open();
		asyncChanel.connect(new InetSocketAddress("localhost", serverPort));
    	System.out.println("created client connection at port " + port);

   		DataReadable dataReadable = new AsynchReader(new DataStore());
		Future<DataRetrievable> future = dataReadable.read(asyncChanel);

		while (!future.isDone()){
			Thread.sleep(100);
		}
		
		return future;
	}
}
